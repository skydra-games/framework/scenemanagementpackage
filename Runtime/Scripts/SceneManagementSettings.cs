#if SKYDRA_EVENTSYSTEM
using Skydra.EventSystem;
#endif
using UnityEngine;

namespace Skydra.SceneManagement
{
    [CreateAssetMenu(fileName = "Game Scenes", menuName = "ML Framework/Game/Scenes Data")]
    public class SceneManagementSettings : ScriptableObject
    {
        public SceneReference mainScene;
        public SceneReference menuScene;
        public SceneReference loadingScene;
#if SKYDRA_EVENTSYSTEM
        public EventID onLoadingRequestEvent;
        public EventID onLoadingStartEvent;
        public EventID onLoadingEndEvent;
#endif
    }
}
