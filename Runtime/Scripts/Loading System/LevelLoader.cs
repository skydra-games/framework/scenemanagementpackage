using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace Skydra.SceneManagement
{
    public static class LevelLoader
    {
        private static LoadingScreen _loadingScreen;
        private static LoadingSequence _sequence;
        private static bool _haveLoadOperation;
        public static bool HaveLoadOperation => _haveLoadOperation;
        private static readonly SceneManagementSettings Settings = Resources.Load<SceneManagementSettings>("SceneManagementSettings");

        // ReSharper disable Unity.PerformanceAnalysis
        public static void StartLoadingOperation(LoadingSequence sequence, string activeScene, LoadingArgs args, UnityAction onLoadingStart = null, UnityAction onLoadingEnd = null)
        {
            if (_haveLoadOperation)
                return;

            _haveLoadOperation = true;
            LoadingOperation(sequence, activeScene, args, onLoadingStart, onLoadingEnd);
        }
        
        private static async void LoadingOperation(LoadingSequence sequence, string activeScene, LoadingArgs args, UnityAction onLoadingStart = null, UnityAction onLoadingEnd = null)
        {
            AsyncOperation async;
            
            // Load additive loading scene
            if (!HaveScene(Settings.loadingScene))
            {
                async = SceneManager.LoadSceneAsync(Settings.loadingScene, LoadSceneMode.Additive);
                while (!async.isDone)
                {
                    await Task.Yield();
                }
            }

#if SKYDRA_EVENTSYSTEM
            EventManager.Raise(Settings.onLoadingRequestEvent);
#endif
            
            // Find loading screen and show it
            _loadingScreen = Object.FindObjectOfType<LoadingScreen>();
            /*
            _loadingScreen.Setup(args);
            _loadingScreen.Initialize();
            */
            bool isLoadingScreenShowed = false;/*
            _loadingScreen.ShowLoading(() => isLoadingScreenShowed = true);*/
            while (!isLoadingScreenShowed)
            {
                await Task.Yield();
            }

#if SKYDRA_EVENTSYSTEM
            EventManager.Raise(Settings.onLoadingStartEvent);
#endif
            onLoadingStart?.Invoke();
            
            sequence.OnUpdate(() =>
            {/*
                _loadingScreen.SetProgress(sequence.Progress);*/
            });/*
            sequence.OnStartNewOperation(() => _loadingScreen.RandomText());
            */

            // When loading screen showed, start sequence and wait for end
            await sequence.StartSequence();
            SceneManager.SetActiveScene(SceneManager.GetSceneByPath(activeScene));
#if SKYDRA_EVENTSYSTEM
            EventManager.Raise(Settings.onLoadingEndEvent);
#endif
            onLoadingEnd?.Invoke();
            
            await Task.Delay(TimeSpan.FromSeconds(0.1f));
            
            // When sequence finished, hide loading screen
            bool isLoadingScreenHided = false;/*
            _loadingScreen.HideLoading(() => isLoadingScreenHided = true);
            */
            while (!isLoadingScreenHided)
            {
                await Task.Yield();
            }
            
            // When loading screen hided, unload loading scene
            async = SceneManager.UnloadSceneAsync(Settings.loadingScene);
            while (!async.isDone)
            {
                await Task.Yield();
            }
            
            _haveLoadOperation = false;
        }

        public static LoadingSequence Sequence()
        {
            if (_sequence == null)
                _sequence = new LoadingSequence();
            
            _sequence.Clear();
            return _sequence;
        }
        
        private static bool HaveScene(string scene)
        {
            int sceneCount = SceneManager.sceneCount;
            for (int i = 0; i < sceneCount; i++)
            {
                if (SceneManager.GetSceneAt(i).path == scene)
                    return true;
            }

            return false;
        }
    }
}

