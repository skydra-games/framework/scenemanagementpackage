﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Skydra.SceneManagement
{
    public sealed class LoadingSequence
    {
        private readonly List<BaseLoadingOperation> _operations = new List<BaseLoadingOperation>();
        private UnityAction onUpdate;
        private UnityAction onStartNewOperation;
        private float sequenceProgress;
        private float delay;

        public float Progress => sequenceProgress;

        public void AppendLoadScene(string scene, LoadSceneMode mode, UnityAction onSceneLoaded = null)
        {
            _operations.Add(new LoadOperation(scene, mode, onSceneLoaded));
        }

        public void AppendUnloadScene(string scene, UnityAction onSceneUnloaded = null)
        {
            _operations.Add(new UnloadOperation(scene, onSceneUnloaded));
        }
        
        public void AppendAsyncTask(Func<Task> task, UnityAction onSceneUnloaded = null)
        {
            _operations.Add(new TaskOperation(task, onSceneUnloaded));
        }

        public void SetDelay(float delay)
        {
            this.delay = delay;
        }
        
        public int OperationsCount()
        {
            return _operations.Count;
        }

        public async Task StartSequence()
        {
            int count = _operations.Count;
            for (int i = 0; i < count; i++)
            {
                if (delay > 0)
                    await Task.Delay(TimeSpan.FromSeconds(delay));
                
                int tempIndex = i;
                _operations[i].OnUpdate += progress =>
                {
                    sequenceProgress = (tempIndex + progress) / count;
                    onUpdate?.Invoke();
                };
                onStartNewOperation?.Invoke();
                await _operations[i].StartOperation();
            }
        }

        public void OnUpdate(UnityAction action)
        {
            onUpdate = action;
        }
        
        public void OnStartNewOperation(UnityAction action)
        {
            onStartNewOperation = action;
        }

        public void Clear()
        {
            _operations.Clear();
            sequenceProgress = 0f;
            delay = 0f;
            onUpdate = null;
            onStartNewOperation = null;
        }
    }
}