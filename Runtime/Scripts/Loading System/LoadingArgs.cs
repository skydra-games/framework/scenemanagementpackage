﻿using System;
using UnityEngine;

namespace Skydra.SceneManagement
{
    [System.Serializable]
    public class LoadingArgs : EventArgs
    {
        public string levelName;
        public string[] tips;
        public Sprite splashScreen;
        public bool tapToStartOnLoadEnd;

        public LoadingArgs(string levelName, string[] tips, Sprite splashScreen, bool tapToStartOnLoadEnd = false)
        {
            this.levelName = levelName;
            this.tips = tips;
            this.splashScreen = splashScreen;
            this.tapToStartOnLoadEnd = tapToStartOnLoadEnd;
        }
    }
}