﻿using System.Threading.Tasks;
using UnityEngine.Events;

namespace Skydra.SceneManagement
{
    public abstract class BaseLoadingOperation
    {
        public UnityAction<float> OnUpdate { get; set; }
        protected readonly UnityAction OnOperationEnd;

        protected BaseLoadingOperation(UnityAction onOperationEnd = null)
        {
            OnOperationEnd = onOperationEnd;
        }


        public abstract Task StartOperation();
    }
}