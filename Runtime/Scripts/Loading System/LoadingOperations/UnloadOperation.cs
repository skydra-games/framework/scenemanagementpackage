﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Skydra.SceneManagement
{
    public class UnloadOperation : BaseLoadingOperation
    {
        private readonly string _scene;

        public UnloadOperation(string scene, UnityAction onOperationEnd) : base(onOperationEnd)
        {
            _scene = scene;
        }

        public override async Task StartOperation()
        {
            AsyncOperation async = SceneManager.UnloadSceneAsync(_scene);
            while (!async.isDone)
            {
                OnUpdate?.Invoke(async.progress);
                await Task.Yield();
            }
            OnUpdate?.Invoke(1f);
            if (OnOperationEnd != null)
            {
                OnOperationEnd.Invoke();
                await Task.Yield();
            }
        }
    }
}