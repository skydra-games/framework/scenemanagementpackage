﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Skydra.SceneManagement
{
    public sealed class LoadOperation : BaseLoadingOperation
    {
        private readonly string _scene;
        private readonly LoadSceneMode _mode;
        private AsyncOperation task;

        public LoadOperation(string scene, LoadSceneMode mode, UnityAction onOperationEnd) : base(onOperationEnd)
        {
            _scene = scene;
            _mode = mode;
        }

        public override async Task StartOperation()
        {
            AsyncOperation async = SceneManager.LoadSceneAsync(_scene, _mode);
            while (!async.isDone)
            {
                OnUpdate?.Invoke(async.progress);
                await Task.Yield();
            }
            OnUpdate?.Invoke(1f);
            if (OnOperationEnd != null)
            {
                OnOperationEnd.Invoke();
                await Task.Yield();
            }
        }
    }
}