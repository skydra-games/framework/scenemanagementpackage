﻿using System;
using System.Threading.Tasks;
using UnityEngine.Events;

namespace Skydra.SceneManagement
{
    public sealed class TaskOperation : BaseLoadingOperation
    {
        private readonly Func<Task> _task;

        public TaskOperation(Func<Task> task, UnityAction onOperationEnd) : base(onOperationEnd)
        {
            _task = task;
        }
        
        public override async Task StartOperation()
        {
            await _task();
        }
    }
}