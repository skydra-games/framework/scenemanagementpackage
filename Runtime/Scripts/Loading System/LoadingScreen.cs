﻿using System;
using System.Collections.Generic;
using System.Linq;/*
using DG.Tweening;
using Sirenix.OdinInspector;*/
//using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Skydra.SceneManagement
{
    [RequireComponent(typeof(CanvasGroup))]
    public class LoadingScreen : MonoBehaviour
    {/*
        public CanvasGroup group;
        public Image fillImage;
        public Image splashScreen;
        
        [ToggleGroup("showLevelName", "Show Level Name")] public bool showLevelName;
        [ToggleGroup("showLevelName", "Show Level Name")] public TextMeshProUGUI levelName;
        
        [ToggleGroup("showLoadingText", "Show Loading Texts")] public bool showLoadingText;
        [ToggleGroup("showLoadingText", "Show Loading Texts")] public TextMeshProUGUI loadingText;
        [ToggleGroup("showLoadingText", "Show Loading Texts")] public List<string> loadingTexts;
        
        [ToggleGroup("showTapToStart", "Tap To Start Screen")] public bool showTapToStart;
        [ToggleGroup("showTapToStart", "Tap To Start Screen")] public TapToStartLoadingPanel tapToStartPanel;
        public Transform scalePivot;
        public float splashScreenScale;
        public float showDuration;
        public float hideDuration;
        public Ease scaleInEase;
        public Ease scaleOutEase;

        private float _progress;
        private Tween _progressTween;
        private Sequence _loadingScreenSeq;
        private List<string> _tempTexts = new List<string>();

        private void Awake()
        {
            scalePivot.localScale = 1.1f * Vector3.one;
            group.alpha = 0f;
        }

        public void Initialize()
        {
            RandomText();
        }

        private void OnDestroy()
        {
            _progressTween?.Kill(true);
            _loadingScreenSeq.Kill(true);
        }

        public void Setup(LoadingArgs args)
        {
            if (showLevelName)
                levelName.text = args.levelName;
            if (showLoadingText && args.tips.Length > 0)
                loadingTexts = args.tips.ToList();
            if (args.splashScreen != null)
                splashScreen.sprite = args.splashScreen;
            showTapToStart = args.tapToStartOnLoadEnd;
        }
        
        public void SetProgress(float value)
        {
            _progressTween?.Kill();
            _progressTween = DOTween.To(() => _progress, x =>
                {
                    _progress = x;
                    if (fillImage)
                        fillImage.fillAmount = _progress;
                }, value, 0.2f)
                .SetEase(Ease.OutSine);
        }
        
        public void RandomText()
        {
            if (loadingTexts.Count == 0 || !showLoadingText)
                return;

            if (_tempTexts.Count == 0)
                RefreshTextPool();
            
            int index = Random.Range(0, _tempTexts.Count);
            loadingText.text = _tempTexts[index];
            _tempTexts.RemoveAt(index);
        }

        private void RefreshTextPool()
        {
            _tempTexts.AddRange(loadingTexts);
        }

        #region Loading Animations
        
        public void ShowLoading(UnityAction onComplete = null)
        {
            LoadingScreenAnimation(1f, 1f,  showDuration,Ease.OutSine, scaleInEase, onComplete);
        }

        public void HideLoading(UnityAction onComplete = null)
        {
            if (showTapToStart)
            {
                tapToStartPanel.Initialize();
                tapToStartPanel.Appear();
                tapToStartPanel.Setup(() =>
                {
                    LoadingScreenAnimation(splashScreenScale, 0, hideDuration, Ease.OutSine, scaleOutEase, onComplete);
                });
            }
            else
            {
                LoadingScreenAnimation(splashScreenScale, 0, hideDuration, Ease.OutSine, scaleOutEase, onComplete);
            }
        }

        private void LoadingScreenAnimation(float scale, float alpha, float duration, Ease fadeEase, Ease scaleEase, UnityAction onComplete = null)
        {
            _loadingScreenSeq?.Kill();
            _loadingScreenSeq = DOTween.Sequence();
            _loadingScreenSeq.Append(scalePivot.DOScale(scale, duration)
                .SetEase(scaleEase));
            _loadingScreenSeq.Join(group.DOFade(alpha, duration)
                .SetEase(fadeEase));
            _loadingScreenSeq.OnComplete(() => onComplete?.Invoke());
        }

        #endregion*/
    }
}